package com.example.ttt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var firstplayer = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener {
            buttonchange(button00)

        }
        button01.setOnClickListener {
            buttonchange(button01)

        }
        button02.setOnClickListener {
            buttonchange(button02)

        }
        button10.setOnClickListener {
            buttonchange(button10)

        }
        button11.setOnClickListener {
            buttonchange(button11)

        }
        button12.setOnClickListener {
            buttonchange(button12)

        }
        button20.setOnClickListener {
            buttonchange(button20)

        }
        button21.setOnClickListener {
            buttonchange(button21)

        }
        button22.setOnClickListener {
            buttonchange(button22)

        }
        reset.setOnClickListener {
            firstplayer = true
            button00.isClickable = true
            button00.text = ""
            button01.isClickable = true
            button01.text = ""
            button02.isClickable = true
            button02.text = ""
            button10.isClickable = true
            button10.text = ""
            button11.isClickable = true
            button11.text = ""
            button12.isClickable = true
            button12.text = ""
            button20.isClickable = true
            button20.text = ""
            button21.isClickable = true
            button21.text = ""
            button22.isClickable = true
            button22.text = ""


        }


    }

    private fun buttonchange(button: Button) {
        if (firstplayer) {
            button.text = "X"
        } else {
            button.text = "O"
        }
        button.isClickable = false
        firstplayer = !firstplayer
        checkwinner()


    }

    private fun checkwinner() {
        if (button00.text.toString()
                .isNotEmpty() && button00.text.toString() == button01.text.toString() && button01.text.toString() == button02.text.toString()
        ) {
            showtoast(button00.text.toString())
            block()
        } else if (button10.text.toString()
                .isNotEmpty() && button10.text.toString() == button11.text.toString() && button11.text.toString() == button12.text.toString()
        ) {
            showtoast(button10.text.toString())
            block()
        } else if (button20.text.toString()
                .isNotEmpty() && button20.text.toString() == button21.text.toString() && button21.text.toString() == button22.text.toString()
        ) {
            showtoast(button20.text.toString())
            block()
        } else if (button00.text.toString()
                .isNotEmpty() && button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString()
        ) {
            showtoast(button00.text.toString())
            block()
        } else if (button01.text.toString()
                .isNotEmpty() && button01.text.toString() == button11.text.toString() && button11.text.toString() == button21.text.toString()
        ) {
            showtoast(button01.text.toString())
            block()
        } else if (button02.text.toString()
                .isNotEmpty() && button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString()
        ) {
            showtoast(button02.text.toString())
            block()
        } else if (button00.text.toString()
                .isNotEmpty() && button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString()
        ) {
            showtoast(button00.text.toString())
            block()
        } else if (button02.text.toString()
                .isNotEmpty() && button02.text.toString() == button11.text.toString() && button11.text.toString() == button20.text.toString()
        ) {
            showtoast(button02.text.toString())
            block()
        } else if (button00.text.toString().isNotEmpty() &&
            button01.text.toString().isNotEmpty() &&
            button02.text.toString().isNotEmpty() &&
            button10.text.toString().isNotEmpty() &&
            button11.text.toString().isNotEmpty() &&
            button12.text.toString().isNotEmpty() &&
            button20.text.toString().isNotEmpty() &&
            button21.text.toString().isNotEmpty() &&
            button22.text.toString().isNotEmpty()
        ) {
            Toast.makeText(this, "NO WINNER", Toast.LENGTH_SHORT).show()
        }


    }

    private fun showtoast(message: String) {
        Toast.makeText(this, "Winner is ${message}", Toast.LENGTH_SHORT).show()

    }

    private fun block() {
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button11.isClickable = false
        button12.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false


    }


}














